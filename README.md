# Selenium Template

A simple example of selenium test written in Python

## Requirements

### Basic requirements (for Mac):

 1. [Homebrew](http://brew.sh/)

 2. Python 2.7

	```
	$ brew install python
	```

 3. Chrome driver

	```
	$ brew install chromedriver
	```


## Setup

```
$ pip install -r requirements.txt
```

## Test run

```
$ python test_script.py
```
